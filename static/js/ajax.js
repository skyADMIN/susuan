/**
 * Created by skyADMIN on 15/9/13.
 */

$(function() {
    $("#register_btn").click(function(){
        var user = $("#newname").val();
        var pass = $("#newpassword").val();
        var repass = $("#rnewpassword").val();
        //alert(user+" "+pass);
        if (user == "") {
            $('<div id="msg" />').html("用户名不能为空！").appendTo('#register_btn').fadeOut(2000);
            $("#newname").focus();
            return false;
        }
        if (pass == "") {
            $('<div id="msg" />').html("密码不能为空！").appendTo('#register_btn').fadeOut(2000);
            $("#newpassword").focus();
            return false;
        }
        if(pass != repass){
            $('<div id="msg" />').html("两次密码不一致！").appendTo('#register_btn').fadeOut(2000);
            $("#newpassword").focus();
            return false;
        }
        $.post("../serverside/register.php?action=register",{
            user:user,
            pass:pass
        },
            function(data,status){
                if(data == 1){
                    //alert("注册成功！请登录！");
                    $('#registerModal').modal('hide');
                    sendMessage("注册成功请登录！")
                    //$('#register_box').modal('hide');
                }else{
                    $('<div id="msg" />').html("该用户名已被注册！").appendTo('#register_btn').fadeOut(2000);
                    $("#newname").focus();
                }
            }
        );
    });
    $('#login_btn').click(function(){
        if(isLogin == 1){
            $('#login_box').modal('hide');
            //requestFullScreen();
            $('#screen1').hide();
            $('#screen2').css({"visibility":"visible"});
            return true;
        }
        var user = $("#username").val();
        var pass = $("#password").val();
        if (user == "") {
            $('<div id="msg" />').html("用户名不能为空！").appendTo('#login_btn').fadeOut(2000);
            $("#user").focus();
            return false;
        }
        if (pass == "") {
            $('<div id="msg" />').html("密码不能为空！").appendTo('#login_btn').fadeOut(2000);
            $("#pass").focus();
            return false;
        }
        //alert(user+" "+pass);
        $.post("../serverside/login.php?action=login",{
                user:user,
                pass:pass
            },
            function(data,status){
                if(data == 1){
                    //alert("登录成功！");
                    sendMessage("登录成功！");
                    isLogin = 1;
                    $('#identity').css({"visibility":"visible"});
                    $('#LoginModal').modal('hide');
                    requestFullScreen();
                    $('#screen1').hide();
                    $('#screen2').css({"visibility":"visible"});
                    $('#user').html(user);
                    $('#time_Model').css({"visibility": "visible"});
                    toast("主界面加载完毕<br>前方高能，非小学生迅速撤离！");
                }else{
                    //alert("登录失败！");
                    sendMessage("登录失败！请检查账号密码！");
                }
            }
        );
    });
});