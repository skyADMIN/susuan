var isToastShow = 0;
var isFullScreen = 0;
var isLogin = 0;
var isfocus = 1;
//var isAnswer = 0;
/*******急速模式的全局变量*************/
var randnumber1;
var randnumber2;
var fuhao;
var result1;
var timer1;
var timer2;
var tishu = 0;
var righttishu = 0;
var isPause = 1;
/************极速模式全局变量止********/
/***********普通模式全局变量**********/
var number1 = new Array(5);
var number2 = new Array(5);
var answer = new Array(5);
var fuhao1 = new Array(5);
var page = -1;
var flag;
/************普通模式全局变量止*******/
/***********难度因子**********/
var level;
var factor1;
var factor2;
var factorfuhao;
var zongtishu = -1;
var timejiange;
var fuhao123 = 0;
//message处理
function sendMessage(msg) {
    $('#message').html(msg);
    $('#messageModal').modal('show');
}


/*********************************************级联菜单*************************************************/
var select1_len = document.frm.s1.options.length;
var select2 = new Array(select1_len);
//把一级菜单都设为数组
for (i = 0; i < select1_len; i++) {
    select2[i] = new Array();
}
//定义基本选项
select2[0][0] = new Option("正常", "m1");
select2[0][1] = new Option("慢速", "m2");
select2[0][2] = new Option("超速", "m3");

select2[1][0] = new Option("5页", "5");
select2[1][1] = new Option("10页", "10");
select2[1][2] = new Option("15页", "15");

//联动函数
function redirec(x) {
    var temp = document.frm.s2;
    for (i = 0; i < select2[x].length; i++) {
        temp.options[i] = new Option(select2[x][i].text, select2[x][i].value);
    }
    temp.options[0].selected = true;
    $('.selectpicker').selectpicker('refresh');
}

//toast处理
function toast(msg) {
    if (isToastShow == 1) {
        setTimeout(function () {
            toast(msg);
        }, 1000);
        return;
    }
    isToastShow = 1;
    setTimeout(function () {
        $('#toast_main').html(msg);
        $('#toast_container').animate({width: 'toggle'});
        $('#toast_container').fadeIn(1);
        setTimeout(function () {
            $('#toast_container').fadeOut(500);
            isToastShow = 0;
        }, 1500);
    }, 100);
}

//全屏相关
//进入全屏
function requestFullScreen() {
    isFullScreen = 1;
    var de = document.body;
    if (de.requestFullscreen) {
        de.requestFullscreen();
    } else if (de.mozRequestFullScreen) {
        de.mozRequestFullScreen();
    } else if (de.webkitRequestFullscreen) {
        de.webkitRequestFullscreen();
    }
}
//退出全屏
function exitFullscreen() {
    isFullScreen = 0;
    var de = document;
    if (de.exitFullscreen) {
        de.exitFullscreen();
    } else if (de.mozCancelFullScreen) {
        de.mozCancelFullScreen();
    } else if (de.webkitCancelFullScreen) {
        de.webkitCancelFullScreen();
    }
}

//关于信息
function about() {
    $('#aboutModal').modal('show');
}

window.onblur = function () {
    if (isfocus == 1) {
        isfocus = 0;
        //alert("请勿点击除本页面以外的地方（为杜绝使用windows计算器辅助计算），当前题目作废，记作一道错题！");
        $('#warningModal').modal('show');
        if (isPause == 0) {
            clearTimeout(timer1);
            clearTimeout(timer2);
            $('#daojishi_miao').html(timejiange);
            $('#pauseti').html("继续答题");
            isPause = 1;
        }
    }
}

window.onfocus = function () {
    isfocus = 1;
}

//难度设定
function setLevel() {
    level = $('#select14').val();
    if (level == 'level1') {
        factor1 = 100;
        factor2 = 10;
        factorfuhao = 2;
    } else if (level == 'level2') {
        factor1 = 100;
        factor2 = 100;
        factorfuhao = 3;
    } else if (level == 'level3') {
        factor1 = 100;
        factor2 = 100;
        factorfuhao = 4;
    } else {
        factor1 = 100;
        factor2 = 100;
        factorfuhao = 4;
    }
    var nandu = $('#select13').val();
    if (nandu == 'm1') {
        timejiange = 5;
    } else if (nandu == 'm2') {
        $('#daojishi_miao').html(10);
        timejiange = 10;
    } else if (nandu == 'm3') {
        $('#daojishi_miao').html(3);
        timejiange = 3;
    }
}


function next() {
    //alert(page);
    var i;
    var k = 0;
    var input = new Array(5);
    for (i = 0; i < 5; i++) {
        number1[i] = parseInt(100 * Math.random());
        number2[i] = parseInt(100 * Math.random());
        fuhao1[i] = parseInt(Math.random() * (3 - 0 + 1) + 0);
    }
    if (flag == 1) {
        input[0] = $('#ipt1').val();
        input[1] = $('#ipt2').val();
        input[2] = $('#ipt3').val();
        input[3] = $('#ipt4').val();
        input[4] = $('#ipt5').val();
        for (i = 0; i < 5; i++) {
            if (input[i] == answer[i]) {
                k++;
            }
        }
        righttishu += k;
        endAnswer1();
        //document.getElementById("commonmode").style.display="none";
        righttishu = 0;
        page = -1;
        flag = 0;
        $('#ipt1').val("");
        $('#ipt2').val("");
        $('#ipt3').val("");
        $('#ipt4').val("");
        $('#ipt5').val("");
        $('#button1').html("重新开始");
        $("#select12").removeAttr("disabled");
        $("#select13").removeAttr("disabled");
        $("#select14").removeAttr("disabled");
        return 0;
    }
    input[0] = $('#ipt1').val();
    input[1] = $('#ipt2').val();
    input[2] = $('#ipt3').val();
    input[3] = $('#ipt4').val();
    input[4] = $('#ipt5').val();
    for (i = 0; i < 5; i++) {
        if (input[i] == answer[i]) {
            k++;
        }
    }
    //alert(k);
    $('#ipt1').val("");
    $('#ipt2').val("");
    $('#ipt3').val("");
    $('#ipt4').val("");
    $('#ipt5').val("");
    righttishu += k;
    if (page > 1) {
        document.getElementById("button1").innerHTML = "下页";
    }
    else if (page == 1) {
        document.getElementById("button1").innerHTML = "提交";
        flag = 1;
    }
    jisuan();
    page--;
}

function go() {
    if (page <= -1) {
        page = $('#select13').val();
        //alert(page);
        $('#button1').html("下页");
        jisuan();
        page--;
    } else {
        //alert(page);
        next();
    }
}

function jisuan() {
    var i;
    var rank = $('#select14').val();
    if (rank == "level1") {
        factor1 = 10;
        factor2 = 10;
        fuhao123 = 1;
    }
    else if (rank == "level2") {
        factor1 = 20;
        factor2 = 20;
        fuhao123 = 1;
    }
    else if (rank == "level3") {
        factor1 = 40;
        factor2 = 40;
    }
    else {
        factor1 = 100;
        factor2 = 100;
    }
    for (i = 0; i < 5; i++) {
        number1[i] = parseInt(factor1 * Math.random());
        number2[i] = parseInt(factor2 * Math.random());
        fuhao1[i] = parseInt(Math.random() * (3 - 0 + 1) + 0);
        if (fuhao123 == 1) {
            fuhao1[i] = parseInt(Math.random() * (1 - 0 + 1) + 0);
        }
    }
    $("#select12").attr("disabled", "disabled");
    $("#select13").attr("disabled", "disabled");
    $("#select14").attr("disabled", "disabled");
    /*first*/
    //document.getElementById("commonmode").style.display="block";
    one();
    two();
    three();
    four();
    five();
}
<!--first-->
function one() {
    if (fuhao1[0] == 0) {
        answer[0] = number1[0] + number2[0];
        $('#number01').html(number1[0]);
        $('#number02').html(number2[0]);
        $('#fuhao0').html("+");
    }
    else if (fuhao1[0] == 1) {
        number1[0] = number2[0] + parseInt(factor1 * Math.random());
        answer[0] = number1[0] - number2[0];
        $('#number01').html(number1[0]);
        $('#number02').html(number2[0]);
        $('#fuhao0').html("-");

    }
    else if (fuhao1[0] == 2 && (!fuhao123)) {
        number1[0] = parseInt((factor1 / 4) * Math.random());
        number2[0] = parseInt((factor2 / 4) * Math.random());
        answer[0] = number1[0] * number2[0];
        $('#number01').html(number1[0]);
        $('#number02').html(number2[0]);
        $('#fuhao0').html("×");

    }
    else if (fuhao1[0] == 3 && (!fuhao123)) {
        number2[0] = parseInt(10 * Math.random()) + 1;
        number1[0] = (parseInt(10 * Math.random()) + 1) * number2[0];
        answer[0] = number1[0] / number2[0];
        $('#number01').html(number1[0]);
        $('#number02').html(number2[0]);
        $('#fuhao0').html("÷");

    }
}
<!--two-->
function two() {
    if (fuhao1[1] == 0) {
        answer[1] = number1[1] + number2[1];
        $('#number11').html(number1[1]);
        $('#number12').html(number2[1]);
        $('#fuhao1').html("+");
    }
    else if (fuhao1[1] == 1) {
        number1[1] = number2[1] + parseInt(factor1 * Math.random());
        answer[1] = number1[1] - number2[1];
        $('#number11').html(number1[1]);
        $('#number12').html(number2[1]);
        $('#fuhao1').html("-");

    }
    else if (fuhao1[1] == 2 && (!fuhao123)) {
        number1[1] = parseInt((factor1 / 4) * Math.random());
        number2[1] = parseInt((factor2 / 4) * Math.random());
        answer[1] = number1[1] * number2[1];
        $('#number11').html(number1[1]);
        $('#number12').html(number2[1]);
        $('#fuhao1').html("×");

    }
    else if (fuhao1[1] == 3 && (!fuhao123)) {
        number2[1] = parseInt(10 * Math.random()) + 1;
        number1[1] = (parseInt(10 * Math.random()) + 1) * number2[1];
        answer[1] = number1[1] / number2[1];
        $('#number11').html(number1[1]);
        $('#number12').html(number2[1]);
        $('#fuhao1').html("÷");
    }
}
<!--three-->
function three() {
    if (fuhao1[2] == 0) {
        answer[2] = number1[2] + number2[2];
        $('#number21').html(number1[2]);
        $('#number22').html(number2[2]);
        $('#fuhao2').html("+");
    }
    else if (fuhao1[2] == 1) {
        number1[2] = number2[2] + parseInt(factor1 * Math.random());
        answer[2] = number1[2] - number2[2];
        $('#number21').html(number1[2]);
        $('#number22').html(number2[2]);
        $('#fuhao2').html("-");

    }
    else if (fuhao1[2] == 2 && (!fuhao123)) {
        number1[2] = parseInt((factor1 / 4) * Math.random());
        number2[2] = parseInt((factor2 / 4) * Math.random());
        answer[2] = number1[2] * number2[2];
        $('#number21').html(number1[2]);
        $('#number22').html(number2[2]);
        $('#fuhao2').html("×");

    }
    else if (fuhao1[2] == 3 && (!fuhao123)) {
        number2[2] = parseInt(10 * Math.random()) + 1;
        number1[2] = (parseInt(10 * Math.random()) + 1) * number2[2];
        answer[2] = number1[2] / number2[2];
        $('#number21').html(number1[2]);
        $('#number22').html(number2[2]);
        $('#fuhao2').html("÷");

    }
}
<!--four-->
function four() {
    if (fuhao1[3] == 0) {
        answer[3] = number1[3] + number2[3];
        $('#number31').html(number1[3]);
        $('#number32').html(number2[3]);
        $('#fuhao3').html("+");
    }
    else if (fuhao1[3] == 1) {
        number1[3] = number2[3] + parseInt(factor1 * Math.random());
        answer[3] = number1[3] - number2[3];
        $('#number31').html(number1[3]);
        $('#number32').html(number2[3]);
        $('#fuhao3').html("-");

    }
    else if (fuhao1[3] == 2 && (!fuhao123)) {
        number1[3] = parseInt((factor1 / 4) * Math.random());
        number2[3] = parseInt((factor2 / 4) * Math.random());
        answer[3] = number1[3] * number2[3];
        $('#number31').html(number1[3]);
        $('#number32').html(number2[3]);
        $('#fuhao3').html("×");

    }
    else if (fuhao1[3] == 3 && (!fuhao123)) {
        number2[3] = parseInt(10 * Math.random()) + 1;
        number1[3] = (parseInt(10 * Math.random()) + 1) * number2[3];
        answer[3] = number1[3] / number2[3];
        $('#number31').html(number1[3]);
        $('#number32').html(number2[3]);
        $('#fuhao3').html("÷");

    }
}
<!--five-->
function five() {
    if (fuhao1[4] == 0) {
        answer[4] = number1[4] + number2[4];
        $('#number41').html(number1[4]);
        $('#number42').html(number2[4]);
        $('#fuhao4').html("+");
    }
    else if (fuhao1[4] == 1) {
        number1[4] = number2[4] + parseInt(factor1 * Math.random());
        answer[4] = number1[4] - number2[4];
        $('#number41').html(number1[4]);
        $('#number42').html(number2[4]);
        $('#fuhao4').html("-");

    }
    else if (fuhao1[4] == 2 && (!fuhao123)) {
        number1[4] = parseInt((factor1 / 4) * Math.random());
        number2[4] = parseInt((factor2 / 4) * Math.random());
        answer[4] = number1[4] * number2[4];
        $('#number41').html(number1[4]);
        $('#number42').html(number2[4]);
        $('#fuhao4').html("×");
    }
    else if (fuhao1[4] == 3 && (!fuhao123)) {
        number2[4] = parseInt(10 * Math.random()) + 1;
        number1[4] = (parseInt(10 * Math.random()) + 1) * number2[4];
        answer[4] = number1[4] / number2[4];
        $('#number41').html(number1[4]);
        $('#number42').html(number2[4]);
        $('#fuhao4').html("÷");
    }
}
//结束答题按钮执行获取日期，题数，正确率，存进数据库
function endAnswer1() {
    if (isLogin == 1) {
        var myDate1 = new Date();
        var mytime1 = myDate1.toLocaleString();
        var tishu1 = 5 * $('#select13').val();
        //alert(tishu1);
        var correctrate1 = righttishu / tishu1;
        correctrate1 = parseInt(correctrate1 * 10000) / 100;
        correctrate1 = correctrate1 + "%";
        var model1;
        //alert($('#select12').val());
        if ($('#select12').val() == 2) {
            model1 = '极速模式';
        } else {
            model1 = '普通模式';
        }
        var level2;
        var level = $('#select14').val();
        if (level == 'level1') {
            level2 = "一年级";
        } else if (level == 'level2') {
            level2 = "二年级";
        } else if (level == 'level3') {
            level2 = "三年级";
        } else {
            level2 = "四年级";
        }
        var a, b, c;
        if (tishu1 <= 0) {
            tishu1 = 0;
            correctrate1 = "0%";
        }

        a = tishu1;
        b = righttishu;
        c = correctrate1;


        //alert(level1+" "+model);
        //alert(righttishu+" "+tishu+" "+correctrate);
        $.post("../serverside/record.php?action=record", {
                datetime: mytime1,
                number: tishu1,
                correctrate: correctrate1,
                model: model1,
                level1: level2
            },
            function (data, status) {
                //alert(data);
                if (data == 1) {
                    //alert("做题记录已储存！");
                    sendMessage("答题完毕，共做" + a + "道题,做对" + b + "道题，正确率为：" + c + "。<br>做题记录已储存！");
                } else {
                    //alert("发生了一个错误！");
                    sendMessage("发生了一个未知错误！");
                }
            }
        )
    } else {
        //alert("您未登录，不能存储做题记录！");
        sendMessage("答题完毕，做对" + righttishu + "道题。<br>您未登录，不能保存做题记录！");
    }

}

/*************************************急速模式的函数***********************************/
//结束答题按钮执行获取日期，题数，正确率，存进数据库
function endAnswer() {
    if (isPause == 1) {
        toast("请先开始答题！");
        return;
    }
    $("#select12").removeAttr("disabled");
    $("#select13").removeAttr("disabled");
    $("#select14").removeAttr("disabled");
    clearTimeout(timer1);
    clearTimeout(timer2);
    if (isLogin == 1) {
        var myDate = new Date();
        var mytime = myDate.toLocaleString();
        tishu--;
        var correctrate = righttishu / tishu;
        correctrate = parseInt(correctrate * 10000) / 100;
        correctrate = correctrate + "%";
        var model;
        //alert($('#select12').val());
        if ($('#select12').val() == 2) {
            model = '极速模式';
        } else {
            model = '普通模式';
        }
        var level1;
        if (level == "level1") {
            level1 = "一年级";
        } else if (level == "level2") {
            level1 = "二年级";
        } else if (level == "level3") {
            level1 = "三年级";
        } else {
            level1 = "四年级";
        }
        var a, b, c;
        if (tishu <= 0) {
            tishu = 0;
            righttishu = 0;
            correctrate = "0%";
        }
        a = tishu;
        b = righttishu;
        c = correctrate;
        //alert(level1+" "+model);
        //alert(righttishu+" "+tishu+" "+correctrate);
        $.post("../serverside/record.php?action=record", {
                datetime: mytime,
                number: tishu,
                correctrate: correctrate,
                model: model,
                level1: level1
            },
            function (data, status) {
                //alert(data);
                if (data == 1) {
                    //alert("做题记录已储存！");
                    sendMessage("本次答题，共答" + a + "道，其中答对题数" + b + "道，正确率为：" + c + "<br>做题记录已储存！");
                } else {
                    //alert("发生了一个错误！");
                    sendMessage("发生了一个未知错误！");
                }
            }
        );
    } else {
        //alert("您未登录，不能存储做题记录！");
        tishu--;
        var correctrate = righttishu / tishu;
        correctrate = parseInt(correctrate * 10000) / 100;
        if (tishu <= 0) {
            tishu = 0;
            righttishu = 0;
            correctrate = "0";
        }
        sendMessage("本次答题，共答" + tishu + "道，其中答对题数" + righttishu + "道，正确率为：" + correctrate + "%<br>您未登录，不能保存做题记录！");
    }
    cleanAnswer();
}

//回答正确题数加一
function answerIsRight() {
    righttishu = $('#rightnumber_ti').html();
    righttishu++;
    $('#rightnumber_ti').html(righttishu);
}

function panduan() {
    var rs = $('#input1').val();
    //alert(rs+" "+result1);
    if (rs == result1) {
        $('<div id="msg1" />').html("回答正确！").appendTo('#main_panel').fadeOut(2000);
        answerIsRight();
        $('#input1').val("");
        $('#input1').focus();
    } else {
        $('<div id="msg1" />').html("回答错误！").appendTo('#main_panel').fadeOut(2000);
        $('#input1').val("");
        $('#input1').focus();
    }
}

function daojishi() {
    var miao = $('#daojishi_miao').html();
    if (miao > 0) {
        miao--;
    } else {
        miao = timejiange;
        clearTimeout(timer2);
    }
    $('#daojishi_miao').html(miao);
    timer2 = setTimeout("daojishi()", 1000);
}

function jisuankaishi() {
    $('#number_ti').html(tishu);
    daojishi();
    randnumber1 = parseInt(factor1 * Math.random());
    randnumber2 = parseInt(factor2 * Math.random());
    fuhao = parseInt(factorfuhao * Math.random());
    if (fuhao == 0) {
        randnumber1 = parseInt(factor1 * Math.random());
        randnumber2 = parseInt(factor2 * Math.random());
        result1 = randnumber1 + randnumber2;
        $('#fuhao').html("+");
    }
    else if (fuhao == 1) {
        randnumber1 = parseInt(factor1 * Math.random());
        randnumber2 = parseInt(randnumber1 * Math.random());
        if (factor2 == 10) {
            var linshi = parseInt(10 * Math.random());
            randnumber2 = (linshi < randnumber1) ? linshi : parseInt(randnumber1 * Math.random());
        }
        result1 = randnumber1 - randnumber2;
        $('#fuhao').html("-");
    } else if (fuhao == 2) {
        if (level == 'level2') {
            randnumber1 = parseInt(10 * Math.random());
            randnumber2 = parseInt(10 * Math.random());
        } else {
            randnumber1 = parseInt(50 * Math.random());
            randnumber2 = parseInt(10 * Math.random());
        }
        result1 = randnumber1 * randnumber2;
        $('#fuhao').html("×");
    } else {
        randnumber2 = parseInt(10 * Math.random()) + 1;
        var beichushu = (parseInt(20 * Math.random()) + 1) * randnumber2;
        randnumber1 = beichushu;
        result1 = randnumber1 / randnumber2;
        $('#fuhao').html("÷");
    }
    $('#randnumber1').html(randnumber1);
    $('#randnumber2').html(randnumber2);
    if (tishu == zongtishu) {
        endAnswer();
    }
    tishu++;
    timer1 = setTimeout("panduan();jisuankaishi()", timejiange * 1000);
}

function pauseAnswer() {
    setLevel();
    if (isPause == 0) {
        clearTimeout(timer1);
        clearTimeout(timer2);
        $('#daojishi_miao').html(5);
        $('#pauseti').html("继续答题");
        isPause = 1;
    } else {
        toast("难度选择已锁定！");
        $("#select12").attr("disabled", "disabled");
        $("#select13").attr("disabled", "disabled");
        $("#select14").attr("disabled", "disabled");
        isPause = 0;
        $('#pauseti').html("暂停答题");
        jisuankaishi();
    }
}

function cleanAnswer() {
    $('#pauseti').html("开始答题");
    clearTimeout(timer1);
    clearTimeout(timer2);
    $('#daojishi_miao').html(5);
    tishu = 0;
    righttishu = 0;
    $('#number_ti').html(tishu);
    $('#rightnumber_ti').html(0);
    isPause = 1;
}


/*********************************************急速模式函数截止*****************************************/

//从数据库中读取做题记录插入recordList中
function loadList() {
    $.post("../serverside/record.php?action=load", {},
        function (data, status) {
            $('#recordList').html(data);
        }
    );
}

function exitLogin() {
    $.post("../serverside/login.php?action=exit", {},
        function (data) {
            if (data == -1) {
                sendMessage("已成功退出登录！");
            }
        }
    )
}

/*chart js*/
var lineChartData;
function loadData() {
    var value1 = parseFloat($("#correctrateData1").html()) * 1;
    var value2 = parseFloat($("#correctrateData2").html()) * 1;
    var value3 = parseFloat($("#correctrateData3").html()) * 1;
    var value4 = parseFloat($("#correctrateData4").html()) * 1;
    var value5 = parseFloat($("#correctrateData5").html()) * 1;
    var value6 = parseFloat($("#correctrateData6").html()) * 1;
    var value7 = parseFloat($("#correctrateData7").html()) * 1;
    var value8 = parseFloat($("#correctrateData8").html()) * 1;
    var value9 = parseFloat($("#correctrateData9").html()) * 1;
    var value10 = parseFloat($("#correctrateData10").html()) * 1;
    lineChartData = {
        labels: ["", "", "", "", "", "", "", "", "", ""],
        datasets: [
            {
                label: "My Second dataset",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [value10, value9, value8, value7, value6, value5, value4, value3, value2, value1]
            }
        ]
    }
}

function chart() {
    var ctx = document.getElementById("canvas").getContext("2d");
    window.myLine = new Chart(ctx).Line(lineChartData, {
        responsive: true
    });
}


function paint() {
    $("#recordModal").modal('hide');
    loadData();
    $("#chart").modal('show');
    setTimeout(function () {
        chart();
    }, 200);
}


$(document).ready(function ($) {

    //$('.selectpicker').selectpicker();
    $('.selectpicker').selectpicker('refresh');

    if (!Modernizr.canvas) {
        popup('还在用古董浏览器么，赶紧更换现代浏览器吧！');
    }
    //第一屏按钮
    $("#enter1").click(function () {
        $('#screen1').hide();
        $('#screen2').css({"visibility": "visible"});
        $('#time_Model').css({"visibility": "visible"});
        toast("主界面加载完毕<br>前方高能，非小学生迅速撤离！");
    });
    //第二屏幕按钮
    //返回首页
    $('#index_btn').click(function () {
        //exitFullscreen();
        if (isLogin == 1) {
            isLogin = 0;
            exitLogin();
        }
        $('#identity').css({"visibility": "hidden"});
        $('#screen2').css({"visibility": "hidden"});
        $('#normal_Model').css({"visibility": "hidden"});
        $('#time_Model').css({"visibility": "hidden"});
        $('#screen1').show();
        cleanAnswer();
    });
    //全屏
    $('#fullscreen_btn').click(function () {
        if (isFullScreen == 0) {
            $('#fullscreen_btn').html('退出全屏');
            requestFullScreen();
        } else {
            $('#fullscreen_btn').html('进入全屏');
            exitFullscreen();
        }
    });
    //回车快速换题
    $('#input1').keydown(function () {
        if (event.keyCode == 13) {
            panduan();
            clearTimeout(timer1);
            clearTimeout(timer2);
            $('#daojishi_miao').html(timejiange);
            jisuankaishi();
        }
    });
    $('#select12').change(function () {
        var model = $('#select12').val();
        if (model == 1) {
            $('#time_Model').css({"visibility": "hidden"});
            $('#normal_Model').css({"visibility": "visible"});
        } else {
            $('#time_Model').css({"visibility": "visible"});
            $('#normal_Model').css({"visibility": "hidden"});
        }
    });

    toast('欢迎小朋友们进入速算训练区！');

});
